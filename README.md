## 联系我

您可以通过以下几种方式联系我。

### 商务合作

商务合作洽谈。

![商务合作请扫描](https://asset.felord.cn/blog/20211228110143.png)

### 高端技术社群

学习前沿流行技术、技巧，和行业大神近距离接触。

![扫码加入社群](https://asset.felord.cn/blog/20211228110342.png)

### 私人助理

投稿、爆料、开白等事宜。

![私事请扫描](https://asset.felord.cn/blog/20211228110039.png)

### 头条号

![](https://asset.felord.cn/blog/20211228112035.jpg)

### 微信视频号

![](https://asset.felord.cn/blog/20211228111950.jpg)

###  我的其它媒体

- **个人网站：**`https://felord.cn`
- **掘金：** `https://juejin.cn/user/4107431172378887`
- **CSDN：**`https://blog.csdn.net/qq_35067322`
- **开源中国：**`https://my.oschina.net/10000000000`
- **博客园：**`https://www.cnblogs.com/felordcn/`
- **知乎：**`https://www.zhihu.com/people/dax-12`
- **思否：**`https://segmentfault.com/u/10000000`

